package com.keithmarsh.jerseyplay;

import com.keithmarsh.jerseyplay.model.User;
import com.keithmarsh.jerseyplay.util.DataEdge;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {
    Logger   log       = Logger.getLogger(this.getClass().getName());
    DataEdge mDataEdge = new DataEdge();
  
    public UserService() {
        log.info("UserService()");
    }

    @GET
    public List<String> get() {
        return mDataEdge.getUserIds();
    }

    @GET
    @Path("{userId}")
    public User get(@PathParam("userId") int userId) {
        return null;
    }

}
