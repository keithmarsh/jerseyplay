package com.keithmarsh.jerseyplay;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.logging.Logger;

import com.keithmarsh.jerseyplay.UserService;

@ApplicationPath("api")
public class KJMResourceConfig extends ResourceConfig {
	Logger log = Logger.getLogger(this.getClass().getName());
   public KJMResourceConfig() {
      log.warning("KJMResourceConfig");
      packages("com.keithmarsh.jerseyplay");
      register(UserService.class);
      //register(new Users());
   }
}
