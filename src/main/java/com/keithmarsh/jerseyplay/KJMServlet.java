package com.keithmarsh.jerseyplay;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "AnnotatedServlet",
    description = "A sample annotated servlet",
    urlPatterns = {"/KJMServlet"}
)
public class KJMServlet extends HttpServlet {
 	 static final long serialVersionUID = 1L;
    Logger log = Logger.getLogger(this.getClass().getName());
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.warning("doGet");
        PrintWriter writer = response.getWriter();
        writer.println("<html>Hello, I am a Java servlet!</html>");
        writer.flush();
    }
}