package com.keithmarsh.jerseyplay.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class User {
   @PrimaryKey
	String id;
	String provider;
	String regStep;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getRegStep() {
		return regStep;
	}

	public void setRegStep(String regStep) {
		this.regStep = regStep;
	}
	
	@Override
	public String toString() {
		return "[User:" + id + "]";
	}
}
