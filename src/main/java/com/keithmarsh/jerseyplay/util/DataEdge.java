package com.keithmarsh.jerseyplay.util;

import org.bson.Document;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DataEdge {
    Logger                    log         = Logger.getLogger(this.getClass().getName());
    MongoCredential           mCredential = null;
    MongoClient               mMongo      = null;
    MongoDatabase             mDB         = null;
    MongoCollection<Document> mDBUsers    = null;
    MongoCollection<Document> mDBClubs    = null;

    public DataEdge() {
        Config config = new Config();
        config.load();
        mCredential = MongoCredential.createCredential(config.get("DBUSER"), config.get("DBNAME"), config.get("DBPASS").toCharArray());
        mMongo = new MongoClient(new ServerAddress(config.get("DBHOST"), Integer.valueOf(config.get("DBPORT"))), Arrays.asList(mCredential));
        if (mMongo != null) {
            mDB = mMongo.getDatabase(config.get("DBNAME"));
            if (mDB != null) {
                mDBUsers = mDB.getCollection("users");
                mDBClubs = mDB.getCollection("clubs");
            }
        }
    }

    public List<String> getUserIds() {
        List<String> ids = new LinkedList<String>();
        log.info("Count " + mDBUsers.count());
        for (Document match : mDBUsers.find()) {
            log.info("Found " + match.toString());
            ids.add(match.getString("_id"));
        }
        return ids;
    }
}
