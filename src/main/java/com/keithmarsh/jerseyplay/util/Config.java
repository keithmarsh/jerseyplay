package com.keithmarsh.jerseyplay.util;

import java.util.Properties;
import java.io.InputStream;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.IOException;

public class Config {
    Logger log = Logger.getLogger(this.getClass().getName());
    private Properties mAppProps = null;
    public void load() {
        if (mAppProps == null) {
			   ClassLoader loader = Config.class.getClassLoader();
				InputStream in = loader.getResourceAsStream("creds.properties");
  			   mAppProps = new Properties();
            try {
				    mAppProps.load(in);
            } catch (IOException e) {
                log.log(Level.SEVERE, "Error loading creds.properties", e);
            }
        }
    }
    public String get(String propName) {
        String value = null;
        if (mAppProps != null) {
            if (mAppProps.containsKey(propName)) {
				    value = mAppProps.getProperty(propName).trim();
			   }
        }
        return value;
    }
}